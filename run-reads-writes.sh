#!/bin/sh

PATH=/home/postgres/pg-checksums/bin:$PATH
WARMUP=1800
DURATION=7200
SCALES="50 300 1000"
NAME='read-write'

# load shared functions
. ./functions.sh

function warmup_cluster
{
	d=$1
	s=$2

	mkdir -p "$d/$s/warmup"

	stats_collector_start "$d/$s/warmup"

	pgbench -c 8 -j 2 -s $s -T $WARMUP -l --aggregate-interval=1 -f reads.sql@9 -f writes.sql@1 pgbench > $d/$s/warmup/pgbench.log 2>&1

	stats_collector_stop

	mv pgbench_log.* $d/$s/warmup
}

function run_benchmark
{
	d=$1
	s=$2

	mkdir -p "$d/$s/benchmark"

	psql -c checkpoint postgres > /dev/null 2>&1

	stats_collector_start "$d/$s/benchmark"

	pgbench -c 8 -j 2 -s $s -T $DURATION -l --aggregate-interval=1 -f reads.sql@9 -f writes.sql@1 pgbench > $d/$s/benchmark/pgbench.log 2>&1

	stats_collector_stop

	mv pgbench_log.* $d/$s/benchmark
}

for s in $SCALES; do

	echo [`date`] "$NAME no-checksums scale=$s init"

	init_cluster $NAME/no-checksums $s ''

	echo [`date`] "$NAME no-checksums scale=$s warmup"

	warmup_cluster $NAME/no-checksums $s

	echo [`date`] "$NAME no-checksums scale=$s benchmark"

	run_benchmark $NAME/no-checksums $s

	echo [`date`] "$NAME no-checksums scale=$s done"

	stop_cluster

        git add	$NAME/no-checksums/$s >	/dev/null 2>&1
        git commit -m "$NAME/no-checksums/$s" >	/dev/null 2>&1
        git push > /dev/null 2>&1

done

for s in $SCALES; do

        echo [`date`] "$NAME checksums scale=$s init"

        init_cluster $NAME/checksums $s '-k'

        echo [`date`] "$NAME checksums scale=$s warmup"

        warmup_cluster $NAME/checksums $s

        echo [`date`] "$NAME checksums scale=$s benchmark"

        run_benchmark $NAME/checksums $s

        echo [`date`] "$NAME checksums scale=$s done"

        stop_cluster

        git add	$NAME/checksums/$s >	/dev/null 2>&1
        git commit -m "$NAME/checksums/$s" >	/dev/null 2>&1
        git push > /dev/null 2>&1

done
