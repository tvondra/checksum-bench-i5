#!/bin/sh

./run-pgbench.sh > pgbench.log 2>&1
./run-skewed.sh > skewed.log 2>&1
./run-skewed-n.sh > skewed-n.log 2>&1
./run-reads-writes.sh > reads-writes.log 2>&1
